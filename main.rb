
## Make a Traffic Intersection!
#
# Write a program that models a traffic intersection.
#
# The intersection is a four-way intersection. Each part of the intersection has four lanes:
#
# - A left turn lane
# - Two middle lanes that go straight
# - A right turn lane
#
# The light can either be red, yellow, or green.
# The left-hand turn lane has its own dedicated light. That light can either be red, yellow, green, or flashing orange (go if no cars are coming the other way).
#
# The UI for this is completely up to you. You could print to a console. You could write to a div. You could make an API call to OpenAI to generate an image based on the state of your program! Whatever you like.
#
# ## The Features
#
# Your program should support the following features (time permitting):
#
# - Create a traffic signal whose lights change on a timer.
# - Model cars arriving at the intersection and traveling through while the light is green.
# - Make the left-hand turn lights on opposite sides of the intersection turn green at the same time, letting cars safely turn left. Make sure the "straight" lights are red. You don't want any accidents!
# - Some traffic lights have sensors underneath the road to detect if there are cars waiting. Make your signal smart! For example, if there are no cars waiting, keep that light red. What if cars begin to arrive? How long do you keep the light red?
# - Add support for a "walk" button at each intersection. When the button is pressed, it should cause the intersection to become clear long enough for a person to walk through it.


class TrafficLight
  @@RED = "RED".freeze
  @@YELLOW = "YELLOW".freeze
  @@GREEN = "GREEN".freeze
  @@ORANGE = "ORANGE".freeze
  def initialize(status: @@RED, location: nil)
    @status = status
    @location = location
  end


  def status=(status)
    @status = status
  end

  def location
    return @location
  end

  def status
    return @status
  end

  def self.RED
    return @@RED
  end

  def self.YELLOW
    return @@YELLOW
  end

  def self.GREEN
    return @@GREEN
  end

  def to_s
    case @status
    when @@RED
      return "🔴"[0]
    when @@YELLOW
      return "🟡"[0]
    when @@GREEN
      return "🟢"[0]
    end
  end
end

class Car
  @@UP = "UP".freeze
  @@DOWN = "DOWN".freeze
  @@LEFT = "LEFT".freeze
  @@RIGHT = "RIGHT".freeze
  def initialize(location = nil, direction = nil)
    @location = location
    @direction = direction
    case @direction
    when @@UP
      @left_hand_turn_info = { 'x' => -5, 'y' => -4, 'new_direction' => @@LEFT }
      @right_hand_turn_info = { 'x' => -3, 'y' => 0, 'new_direction' => @@RIGHT }
      @traffic_light_group = 'bottom'
    when @@DOWN
      @left_hand_turn_info = { 'x' => 5, 'y' => 4, 'new_direction' => @@RIGHT }
      @right_hand_turn_info = { 'x' => 3, 'y' => 0, 'new_direction' => @@LEFT }
      @traffic_light_group = 'top'
    when @@LEFT
      @left_hand_turn_info = { 'x' => 4, 'y' => -5, 'new_direction' => @@DOWN }
      @right_hand_turn_info = { 'x' => 0, 'y' => -3, 'new_direction' => @@UP }
      @traffic_light_group = 'right'
    when @@RIGHT
      @left_hand_turn_info = { 'x' => -4, 'y' => 5, 'new_direction' => @@UP }
      @right_hand_turn_info = { 'x' => 0, 'y' => 3, 'new_direction' => @@DOWN }
      @traffic_light_group = 'left'
    end
  end

  # extra step is for when traversing a square that is occupied by a traffic light
  def next_location(extra_step = 0)
    case @direction
    when @@UP
      return [@location[0] - 1 - extra_step, @location[1]]
    when @@DOWN
      return [@location[0] + 1 + extra_step, @location[1]]
    when @@LEFT
      return [@location[0], @location[1] - 1 - extra_step]
    when @@RIGHT
      return [@location[0], @location[1] + 1 + extra_step]
    end
  end

  def direction
    return @direction
  end

  def left_hand_turn_info
    return @left_hand_turn_info
  end

  def right_hand_turn_info
    return @right_hand_turn_info
  end
  def traffic_light_group
    return @traffic_light_group
  end

  def location
    return @location
  end

  def location=(location)
    @location = location
  end

  def direction=(direction)
    @direction = direction
  end

  def self.UP
    return @@UP
  end

  def self.DOWN
    return @@DOWN
  end

  def self.LEFT
    return @@LEFT
  end
  def self.RIGHT
    return @@RIGHT
  end

  def to_s
    return "🚗"[0]
  end


end


class Intersection
  EMPTY_SPOT = ". ".freeze
  def initialize(size = 16)
    @intersection = Array.new(size) { Array.new(size, EMPTY_SPOT)}
    @lights = {
      'top' => {},
      'bottom' => {},
      'left' => {},
      'right' => {}
    }
    @cars = []
  end

  def print_intersection
    @intersection.each do |row|
      puts row.join("  ")
    end
  end

  def lights
    return @lights
  end

  def intersection
    return @intersection
  end

  def put_car(location, car)
    # square already occupied by a car
    return if @intersection[location[0]][location[1]].is_a?(Car)
    car.location = location
    @cars << car
    @intersection[location[0]][location[1]] = car
  end

  def put_traffic_light(location, traffic_light, group)
    group_1, group_2 = group.to_s.split("_")
    @intersection[location[0]][location[1]] = traffic_light
    @lights[group_1][group_2] = traffic_light
  end

  def move_cars
    @cars.each do |car|
      move_car(car)
    end
  end

  def change_traffic_lights()
    if @lights['top']['straight'].status == TrafficLight.GREEN && @lights['bottom']['straight'].status == TrafficLight.GREEN
      @lights['top']['straight'].status = TrafficLight.YELLOW
      @lights['top']['right'].status = TrafficLight.YELLOW
      @lights['bottom']['straight'].status = TrafficLight.YELLOW
      @lights['bottom']['right'].status = TrafficLight.YELLOW
    elsif @lights['top']['straight'].status == TrafficLight.YELLOW && @lights['bottom']['straight'].status == TrafficLight.YELLOW
      @lights['top']['straight'].status = TrafficLight.RED
      @lights['top']['right'].status = TrafficLight.RED
      @lights['bottom']['straight'].status = TrafficLight.RED
      @lights['bottom']['right'].status = TrafficLight.RED

      @lights['top']['left'].status = TrafficLight.GREEN
      @lights['bottom']['left'].status = TrafficLight.GREEN
    elsif @lights['top']['left'].status == TrafficLight.GREEN && @lights['bottom']['left'].status == TrafficLight.GREEN
      @lights['top']['left'].status = TrafficLight.YELLOW
      @lights['bottom']['left'].status = TrafficLight.YELLOW
    elsif @lights['top']['left'].status == TrafficLight.YELLOW && @lights['bottom']['left'].status == TrafficLight.YELLOW
      @lights['top']['left'].status = TrafficLight.RED
      @lights['bottom']['left'].status = TrafficLight.RED

      @lights['left']['straight'].status = TrafficLight.GREEN
      @lights['left']['right'].status = TrafficLight.GREEN
      @lights['right']['straight'].status = TrafficLight.GREEN
      @lights['right']['right'].status = TrafficLight.GREEN
    elsif @lights['left']['straight'].status == TrafficLight.GREEN && @lights['right']['straight'].status == TrafficLight.GREEN
      @lights['left']['straight'].status = TrafficLight.YELLOW
      @lights['left']['right'].status = TrafficLight.YELLOW
      @lights['right']['straight'].status = TrafficLight.YELLOW
      @lights['right']['right'].status = TrafficLight.YELLOW
    elsif @lights['left']['straight'].status == TrafficLight.YELLOW && @lights['right']['straight'].status == TrafficLight.YELLOW
      @lights['left']['straight'].status = TrafficLight.RED
      @lights['left']['right'].status = TrafficLight.RED
      @lights['right']['straight'].status = TrafficLight.RED
      @lights['right']['right'].status = TrafficLight.RED

      @lights['left']['left'].status = TrafficLight.GREEN
      @lights['right']['left'].status = TrafficLight.GREEN
    elsif @lights['left']['left'].status == TrafficLight.GREEN && @lights['right']['left'].status == TrafficLight.GREEN
      @lights['left']['left'].status = TrafficLight.YELLOW
      @lights['right']['left'].status = TrafficLight.YELLOW
    elsif @lights['left']['left'].status == TrafficLight.YELLOW && @lights['right']['left'].status == TrafficLight.YELLOW
      @lights['left']['left'].status = TrafficLight.RED
      @lights['right']['left'].status = TrafficLight.RED

      @lights['top']['straight'].status = TrafficLight.GREEN
      @lights['top']['right'].status = TrafficLight.GREEN
      @lights['bottom']['straight'].status = TrafficLight.GREEN
      @lights['bottom']['right'].status = TrafficLight.GREEN
    end
  end
  private

  def move_car(car)
    # check for map limits
    if car.next_location[0] < 0 || car.next_location[0] >= @intersection.length || car.next_location[1] < 0 || car.next_location[1] >= @intersection.length
      remove_car(car)
      return
    end

    # check next location for lights/cars
    intersection_next_location = @intersection[car.next_location[0]][car.next_location[1]]
    next_is_traffic_light = intersection_next_location.is_a?(TrafficLight)
    next_is_straight_traffic_light = intersection_next_location == @lights[car.traffic_light_group]['straight']
    next_is_left_traffic_light = intersection_next_location == @lights[car.traffic_light_group]['left']
    next_is_right_traffic_light = intersection_next_location == @lights[car.traffic_light_group]['right']
    next_is_car = intersection_next_location.is_a?(Car)

    return if next_is_left_traffic_light && @lights[car.traffic_light_group]['left'].status == TrafficLight.RED
    return if next_is_right_traffic_light && @lights[car.traffic_light_group]['right'].status == TrafficLight.RED

    if next_is_left_traffic_light
      make_turn(car, Car.LEFT)
      return
    end

    if next_is_right_traffic_light
      make_turn(car, Car.RIGHT)
      return
    end

    extra_step = 0
    extra_step = 1 if next_is_traffic_light
    return if @lights[car.traffic_light_group]['straight'].status == TrafficLight.RED && next_is_straight_traffic_light
    return if next_is_car
    @intersection[car.location[0]][car.location[1]] = EMPTY_SPOT
    new_location = car.next_location(extra_step)
    car.location = new_location
    @intersection[car.location[0]][car.location[1]] = car
  end

  def remove_car(car)
    @intersection[car.location[0]][car.location[1]] = EMPTY_SPOT
    @cars.delete(car)
  end

  def make_turn(car, direction)
    turn_info = car.right_hand_turn_info if direction == Car.RIGHT
    turn_info = car.left_hand_turn_info if direction == Car.LEFT
    @intersection[car.location[0]][car.location[1]] = EMPTY_SPOT
    new_location = [car.location[0] + turn_info['x'], car.location[1] + turn_info['y']]
    car.location = new_location
    car.direction = turn_info['new_direction']
    @intersection[car.location[0]][car.location[1]] = car
  end
end


def setup
  intersection = Intersection.new
  mid = intersection.intersection.length / 2
  intersection.put_traffic_light([mid - 4, mid - 2], TrafficLight.new(status: TrafficLight.GREEN), :top_straight)
  intersection.put_traffic_light([mid - 4, mid - 1], TrafficLight.new, :top_left)
  intersection.put_traffic_light([mid - 4, mid - 3], TrafficLight.new, :top_right)

  intersection.put_traffic_light([mid - 2, mid + 2], TrafficLight.new, :right_straight)
  intersection.put_traffic_light([mid - 1, mid + 2], TrafficLight.new, :right_left)
  intersection.put_traffic_light([mid - 3, mid + 2], TrafficLight.new, :right_right)

  intersection.put_traffic_light([mid - 1, mid - 4], TrafficLight.new, :left_left)
  intersection.put_traffic_light([mid, mid - 4], TrafficLight.new, :left_straight)
  intersection.put_traffic_light([mid + 1, mid -4], TrafficLight.new, :left_right)

  intersection.put_traffic_light([mid + 2, mid + 1], TrafficLight.new, :bottom_right)
  intersection.put_traffic_light([mid + 2, mid - 1], TrafficLight.new, :bottom_left)
  intersection.put_traffic_light([mid + 2, mid], TrafficLight.new(status: TrafficLight.GREEN), :bottom_straight)
  return intersection
end

intersection = setup

timer = 0
VALID_STARTING_LOCATIONS = [
  { location: [0, 5], direction: Car.DOWN },
  { location: [0, 6], direction: Car.DOWN },
  { location: [0, 7], direction: Car.DOWN },
  { location: [7, 0], direction: Car.RIGHT },
  { location: [8, 0], direction: Car.RIGHT },
  { location: [9, 0], direction: Car.RIGHT },
  { location: [15, 7], direction: Car.UP },
  { location: [15, 8], direction: Car.UP },
  { location: [15, 9], direction: Car.UP },
  { location: [5, 15], direction: Car.LEFT },
  { location: [6, 15], direction: Car.LEFT },
  { location: [7, 15], direction: Car.LEFT }
]

loop do
  puts "-" * 46
  intersection.print_intersection
  intersection.change_traffic_lights if (timer % 10).zero?
  car_location = VALID_STARTING_LOCATIONS.sample
  intersection.move_cars
  intersection.put_car(car_location[:location], Car.new(car_location[:location], car_location[:direction])) if (timer % 3).zero?
  sleep(ARGV[0].to_f)
  timer += 1
end
