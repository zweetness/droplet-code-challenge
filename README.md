How to run:

```
git clone https://gitlab.com/zweetness/droplet-code-challenge.git
cd droplet-code-challenge
ruby main.rb 0.5
```

To make the simulation run faster, make the value lower.

To make the simulation run slower, make the value higher.

How to install ruby if you don't have it: https://www.ruby-lang.org/en/documentation/installation/

Notes:

When you run the program, it will constantly write out the contents a 2-d array which represents the intersection.
You should see something like this:

![img.png](/img.png)

This is a top down view of the intersection.\
Cars from the bottom move up, from the top move down, from the left move right, and from the right move left.

Cars are randomly placed on the array given a set of valid starting points and directions. 

Design:

I have 3 objects: Traffic Light, Car, and Intersection

The TrafficLight is pretty simple, it keeps track of what light it is showing.

I wanted the Intersection object to more or less orchestrate what happened within itself.

So you'll see in the main loop, for the most part it is calling methods on the Intersection object to perform some sort of operation on itself.

The Car object contains information about its whereabouts, how to perform left and right turn, and the direction it is going. This allows the intersection object to then simply say "Hey car - it's your turn to move". And then the car can give context of where it is so that the intersection knows how to update itself.

Some things I struggled with:

Setting up the 2d array such that intersection is right in the middle and dynamically calculating where in the array the stoplights should be placed/where valid starting places for the cars are.
I'm sure if I had more time, I could do some better arithmetic to calculate these value. For now, they are somewhat hard-coded.

I'm not super happy with my change_traffic_lights method. It's just a huge else if based on the current state of the lights.
Seems like a state machine where I could have different states (top and bottom straight lights are green) and then have a method to "transition" to the next state (top and bottom straight lights yellow)

I needed some more time to print out turns. You'll notice that for left hand turns, the cars teleport to the other side of the intersection.

I am happy with how the objects turned out with the data they encapsulate and methods they perform on that data.

I am happy how the intersection object can say "move_car" and that for any car moving in any direction anywhere on the map, it can figure out how to move it. 